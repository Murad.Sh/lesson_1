package com.shukur.lesson1.service;

import com.shukur.lesson1.model.dto.ProductDto;
import com.shukur.lesson1.model.entity.ProductEntity;
import com.shukur.lesson1.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    @Autowired
    ProductRepository repository;

    public void saveProduct(ProductDto dto) {
        try {
            ProductEntity product = ProductEntity.fromDto(dto);
            repository.save(product);
        } catch (Exception e) {
            System.err.println("Error while saving preference: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void deleteById(Long id) {
        try {
            repository.deleteById(id);
        } catch (Exception e) {
            System.err.println("Error while deleting preference: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public ProductDto  updateById(Long id, ProductDto dto) {
        try {
            Optional<ProductEntity> product = repository.findById(id);
            if (product.isPresent()) {
                ProductEntity updatedProduct = product.get();
                updatedProduct.setName(dto.getName());
                updatedProduct.setDescription(dto.getDescription());
                updatedProduct.setCategory(dto.getCategory());
                updatedProduct.setPrice(dto.getPrice());
                updatedProduct.setQuantity(dto.getQuantity());
                repository.save(updatedProduct);
                return ProductDto.fromEntity(updatedProduct);
            } else {
                return null;
            }
        } catch (Exception e) {
            System.err.println("Error while deleting preference: " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    public Optional<ProductDto> findProductById(Long id) {
        try {
            Optional<ProductEntity> product = repository.findById(id);
            return product.map(ProductDto::fromEntity);
        } catch (Exception e) {
            System.err.println("Error while fetching preferences: " + e.getMessage());
            e.printStackTrace();
            return Optional.empty();
        }
    }

    public List<ProductDto> findAllProducts() {
        try {
            List<ProductEntity> products = repository.findAll();
            return fromListOfEntities(products);
        } catch (Exception e) {
            System.err.println("Error while fetching preferences: " + e.getMessage());
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public List<ProductDto> findProductByName(String name) {
        try {
            List<ProductEntity> products = repository.findByName(name);
            return fromListOfEntities(products);
        } catch (Exception e) {
            System.err.println("Error while fetching preferences: " + e.getMessage());
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public List<ProductDto> findProductByCategory(String category) {
        try {
            List<ProductEntity> products = repository.findByCategory(category);
            return fromListOfEntities(products);
        } catch (Exception e) {
            System.err.println("Error while fetching preferences: " + e.getMessage());
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public List<ProductDto> findProductByPrice(int price) {
        try {
            List<ProductEntity> products = repository.findByPrice(price);
            return fromListOfEntities(products);
        } catch (Exception e) {
            System.err.println("Error while fetching preferences: " + e.getMessage());
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public List<ProductDto> findProductByPriceInterval(int startPrice, int endPrice) {
        try {
            List<ProductEntity> products = repository.findByPriceBetween(startPrice, endPrice);
            return fromListOfEntities(products);
        } catch (Exception e) {
            System.err.println("Error while fetching preferences: " + e.getMessage());
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public List<ProductDto> findProductByNameAndCategory(String name, String category) {
        try {
            List<ProductEntity> products = repository.findByNameAndCategory(name, category);
            return fromListOfEntities(products);
        } catch (Exception e) {
            System.err.println("Error while fetching preferences: " + e.getMessage());
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public List<ProductDto> findProductByQuantity(int quantity) {
        try {
            List<ProductEntity> products = repository.findByQuantity(quantity);
            return fromListOfEntities(products);
        } catch (Exception e) {
            System.err.println("Error while fetching preferences: " + e.getMessage());
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public List<ProductDto> findProductByQuantityBefore(int lastQuantity) {
        try {
            List<ProductEntity> products = repository.findByQuantityBefore(lastQuantity);
            return fromListOfEntities(products);
        } catch (Exception e) {
            System.err.println("Error while fetching preferences: " + e.getMessage());
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public List<ProductDto> fromListOfEntities (List<ProductEntity> products) {
        List<ProductDto> dtos = new ArrayList<>();
        for (ProductEntity product : products) {
            ProductDto dto = ProductDto.fromEntity(product);
            dtos.add(dto);
        }
        return dtos;
    }
}
