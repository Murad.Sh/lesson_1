package com.shukur.lesson1.model.dto;

import com.shukur.lesson1.model.entity.ProductEntity;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ProductDto {
    String name;
    String description;
    String category;
    int price;
    int quantity;

    public static ProductDto fromEntity(ProductEntity entity) {
        ProductDto dto = new ProductDto();
        dto.name = entity.getName();
        dto.description = entity.getDescription();
        dto.category = entity.getCategory();
        dto.price = entity.getPrice();
        dto.quantity = entity.getQuantity();
        return dto;
    }
}
