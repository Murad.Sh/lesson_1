package com.shukur.lesson1.controller;

import com.shukur.lesson1.model.dto.ProductDto;
import com.shukur.lesson1.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    ProductService service;

    private static final Logger logger = LoggerFactory.getLogger(ProductController.class);

    @GetMapping
    public ResponseEntity<List<ProductDto>> findAllProducts() {
        try {
            logger.info("Fetching all products");
            List<ProductDto> products = service.findAllProducts();
            if (products.isEmpty()) {
                logger.info("No products found");
                return ResponseEntity.noContent().build();
            } else {
                logger.info("Products found: {}", products.size());
                return ResponseEntity.ok(products);
            }
        } catch (Exception e) {
            logger.error("Error occurred while fetching products", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/name")
    public ResponseEntity<List<ProductDto>> findAllProductsByName(@RequestParam String name) {
        try {
            logger.info("Fetching all products by name {}", name);
            List<ProductDto> products = service.findProductByName(name);
            if (products.isEmpty()) {
                logger.info("No products found by the name {}", name);
                return ResponseEntity.noContent().build();
            } else {
                logger.info("Found {} products with the name {} ", products.size(), name);
                return ResponseEntity.ok(products);
            }
        } catch (Exception e) {
            logger.error("Error occurred while fetching products with the name {}", name, e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/category")
    public ResponseEntity<List<ProductDto>> findAllProductsByCategory(@RequestParam String category) {
        try {
            logger.info("Fetching all products by category {}", category);
            List<ProductDto> products = service.findProductByCategory(category);
            if (products.isEmpty()) {
                logger.info("No products found in the category {}", category);
                return ResponseEntity.noContent().build();
            } else {
                logger.info("Found {} products in the category {} ", products.size(), category);
                return ResponseEntity.ok(products);
            }
        } catch (Exception e) {
            logger.error("Error occurred while fetching products with the category {}", category, e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/price")
    public ResponseEntity<List<ProductDto>> findAllProductsByPrice(@RequestParam int price) {
        try {
            logger.info("Fetching all products by price {}", price);
            List<ProductDto> products = service.findProductByPrice(price);
            if (products.isEmpty()) {
                logger.info("No products found with the price {}", price);
                return ResponseEntity.noContent().build();
            } else {
                logger.info("Found {} products with the price {} ", products.size(), price);
                return ResponseEntity.ok(products);
            }
        } catch (Exception e) {
            logger.error("Error occurred while fetching products with the price {}", price, e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/priceinterval")
    public ResponseEntity<List<ProductDto>> findAllProductsByPriceInterval(@RequestParam int startPrice, @RequestParam int endPrice) {
        try {
            logger.info("Fetching all products by price interval from {} to {}", startPrice, endPrice);
            List<ProductDto> products = service.findProductByPriceInterval(startPrice, endPrice);
            if (products.isEmpty()) {
                logger.info("No products found in the price range from {} to {}", startPrice, endPrice);
                return ResponseEntity.noContent().build();
            } else {
                logger.info("Found {} products in the price range from {} to {}", products.size(), startPrice, endPrice);
                return ResponseEntity.ok(products);
            }
        } catch (Exception e) {
            logger.error("Error occurred while fetching products in the price range from {} to {}", startPrice, endPrice, e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/nameandcategory")
    public ResponseEntity<List<ProductDto>> findAllProductsByNameAndCategory(@RequestParam String name, @RequestParam String category) {
        try {
            logger.info("Fetching all products by name {} and category {}", name, category);
            List<ProductDto> products = service.findProductByNameAndCategory(name, category);
            if (products.isEmpty()) {
                logger.info("No products found by the name {} ant in the category {}", name, category);
                return ResponseEntity.noContent().build();
            } else {
                logger.info("Found {} products with the name {} and in the category {} ", products.size(), name, category);
                return ResponseEntity.ok(products);
            }
        } catch (Exception e) {
            logger.error("Error occurred while fetching products with the name {} and category {}", name, category, e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/quantity")
    public ResponseEntity<List<ProductDto>> findAllProductsByQuantity(@RequestParam int quantity) {
        try {
            logger.info("Fetching all products by quantity {}", quantity);
            List<ProductDto> products = service.findProductByQuantity(quantity);
            if (products.isEmpty()) {
                logger.info("No products found by the quantity {} ", quantity);
                return ResponseEntity.noContent().build();
            } else {
                logger.info("Found {} products with the quantity {} ", products.size(), quantity);
                return ResponseEntity.ok(products);
            }
        } catch (Exception e) {
            logger.error("Error occurred while fetching products with the quantity {}", quantity, e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/quantitybefore")
    public ResponseEntity<List<ProductDto>> findAllProductsByQuantityBefore(@RequestParam int quantityBefore) {
        try {
            logger.info("Fetching all products with quantity under {}", quantityBefore);
            List<ProductDto> products = service.findProductByQuantityBefore(quantityBefore);
            if (products.isEmpty()) {
                logger.info("No products found with the quantity under {} ", quantityBefore);
                return ResponseEntity.noContent().build();
            } else {
                logger.info("Found {} products with the quantity under {} ", products.size(), quantityBefore);
                return ResponseEntity.ok(products);
            }
        } catch (Exception e) {
            logger.error("Error occurred while fetching products with the quantity under {}", quantityBefore, e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping
    public ResponseEntity<ProductDto> createNewProduct(@RequestBody ProductDto dto) {
        try {
            logger.info("Creating new product {}", dto.toString());
            service.saveProduct(dto);
            logger.info("Created new product {} ", dto.toString());
            return ResponseEntity.ok(dto);
        } catch (Exception e) {
            logger.error("Error occurred while creating product {}", dto, e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

    }

    @PutMapping("/{id}")
    public ResponseEntity<ProductDto> updateProduct(@PathVariable Long id, @RequestBody ProductDto dto) {
        try {
            logger.info("Updating product with id {} to the {} ", id, dto.toString());
            if (service.findProductById(id).isPresent()) {
                ProductDto updatedPromo = service.updateById(id, dto);
                logger.info("Updated product with id {} to {} ", id, dto.toString());
                return ResponseEntity.ok(updatedPromo);
            } else {
                logger.info("Error occurred while updating, because product with id {} is not found ", id);
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            }

        } catch (Exception e) {
            logger.error("Error occurred while updating product with id {} to {} ", id, dto.toString(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable Long id) {
        try {
            logger.info("Deleting product with id {}", id);
            if (service.findProductById(id).isPresent()) {
                service.deleteById(id);
                logger.info("Deleted product with id {} ", id);
                return ResponseEntity.ok("Product is successfully deleted");
            } else {
                logger.info("Error occurred while deleting, because product with id {} is not found ", id);
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            }
        } catch (Exception e) {
            logger.error("Error occurred while deleting product with id {} ", id, e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
