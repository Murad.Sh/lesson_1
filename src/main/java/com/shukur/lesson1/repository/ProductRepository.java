package com.shukur.lesson1.repository;

import com.shukur.lesson1.model.entity.ProductEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity, Long> {
    List<ProductEntity> findByName(String name);
    List<ProductEntity> findByCategory(String category);
    List<ProductEntity> findByPrice(int price);
    List<ProductEntity> findByPriceBetween(int startPrice, int endPrice);
    List<ProductEntity> findByNameAndCategory(String name, String category);
    List<ProductEntity> findByQuantity(int quantity);
    List<ProductEntity> findByQuantityBefore(int lastQuantity);
}
